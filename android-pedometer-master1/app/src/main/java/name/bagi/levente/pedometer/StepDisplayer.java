/*
 *  Pedometer - Android App
 *  Copyright (C) 2009 Levente Bagi
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package name.bagi.levente.pedometer;

import java.util.ArrayList;

/**
 * Counts steps provided by StepDetector and passes the current
 * step count to the activity.
 */
class StepDisplayer implements StepListener {

    private int mCounts = 0;

    StepDisplayer() {
        notifyListener();
    }

    void setSteps(int steps) {
        mCounts = steps;
        notifyListener();
    }
    public void onStep() {
        mCounts++;
        notifyListener();
    }
    void reloadSettings() {
        notifyListener();
    }

    //-----------------------------------------------------
    // Listener
    
    interface Listener {
        void stepsChanged(int value);
        void passValue();
    }
    private ArrayList<Listener> mListeners = new ArrayList<Listener>();

    void addListener(Listener l) {
        mListeners.add(l);
    }
    private void notifyListener() {
        for (Listener listener : mListeners) {
            listener.stepsChanged(mCounts);
        }
    }

}
