/*
 *  Pedometer - Android App
 *  Copyright (C) 2009 Levente Bagi
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package name.bagi.levente.pedometer;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
    private static final String TAG = "MainActivity";
    private PedometerSettings mPedometerSettings;
    private TextView mStepValueView;
    private TextView mStepRealValueView;
    private TextView mPercent;

    private int mStepValue;

    private boolean mIsRunning;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mStepValue = 0;

    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(this);
        mPedometerSettings = new PedometerSettings(mSettings);

        mIsRunning = mPedometerSettings.isServiceRunning();

        if (!mIsRunning) {
            startStepService();
            bindStepService();
        } else {
            bindStepService();
        }

        mPedometerSettings.clearServiceRunning();

        mStepValueView = (TextView) findViewById(R.id.step_value);
        mStepRealValueView = (TextView) findViewById(R.id.distance_value);
        mPercent = (TextView) findViewById(R.id.desired_pace_value);

        Button button1 = (Button) findViewById(R.id.button_clear);
        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mStepRealValueView.setText("");
                resetValues(true);
            }
        });
        Button button2 = (Button) findViewById(R.id.button_calculate);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                final String real = mStepRealValueView.getText().toString();
                final String step = mStepValueView.getText().toString();

                if (real.equals("") || step.equals("")) {
                    return;
                }

                double realDouble = Double.parseDouble(real);
                double stepDouble = Double.parseDouble(step);

                double percent = (stepDouble*100)/realDouble;

                mPercent.setText("" + round(percent) + "%");
            }
        });

        Button button3 = (Button) findViewById(R.id.button_pause);
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                unbindStepService();
                stopStepService();
            }
        });
        Button button4 = (Button) findViewById(R.id.button_play);
        button4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startStepService();
                bindStepService();
            }
        });
    }

    public static double round(double value) {
        long factor = (long) Math.pow(10, 2);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    @Override
    protected void onPause() {
        if (mIsRunning) {
            unbindStepService();
        }
        super.onPause();
    }

    private StepService mService;

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = ((StepService.StepBinder) service).getService();
            mService.registerCallback(mCallback);
            mService.reloadSettings();
        }

        public void onServiceDisconnected(ComponentName className) {
            mService = null;
        }
    };

    private void startStepService() {
        if (!mIsRunning) {
            mIsRunning = true;
            startService(new Intent(MainActivity.this, StepService.class));
        }
    }

    private void bindStepService() {
        bindService(new Intent(MainActivity.this, StepService.class), mConnection, Context.BIND_AUTO_CREATE + Context.BIND_DEBUG_UNBIND);
    }

    private void unbindStepService() {
        unbindService(mConnection);
    }

    private void stopStepService() {
        if (mService != null) {
            stopService(new Intent(MainActivity.this, StepService.class));
        }
        mIsRunning = false;
    }

    private void resetValues(boolean updateDisplay) {
        if (mService != null && mIsRunning) {
            mService.resetValues();
        } else {
            mStepValueView.setText("0");
            SharedPreferences state = getSharedPreferences("state", 0);
            SharedPreferences.Editor stateEditor = state.edit();
            if (updateDisplay) {
                stateEditor.putInt("steps", 0);
                stateEditor.commit();
            }
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        menu.add(0, 8, 0, R.string.settings).setIcon(android.R.drawable.ic_menu_preferences).setShortcut('8', 's').setIntent(new Intent(this, Settings.class));
        return true;
    }

    private StepService.ICallback mCallback = new StepService.ICallback() {
        public void stepsChanged(int value) {
            mHandler.sendMessage(mHandler.obtainMessage(STEPS_MSG, value, 0));
        }
    };

    private static final int STEPS_MSG = 1;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case STEPS_MSG:
                    mStepValue = (int) msg.arg1;
                    mStepValueView.setText("" + mStepValue);
                    break;
                default:
                    super.handleMessage(msg);
            }
        }

    };

}